/*
 * Reference:
 *  https://blog.csdn.net/baidu_38492440/article/details/78193766
 */

import axios from 'axios'
import qs from 'qs'

let _BASE_URL = 'http://118.31.52.226/chromit'

axios.defaults.withCredentials=true

// 请求方式的配置
export default {
    post(url, data) {  //  post
        return axios({
            method: 'post',
            baseURL: _BASE_URL,
            url,
            data: qs.stringify(data),
            timeout: 5000,
            headers: {
                'X-Requested-With': 'XMLHttpRequest',
                'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
            }
        })
    },
    get(url, params) {  // get
        return axios({
            method: 'get',
            baseURL: _BASE_URL,
            url,
            params, // get 请求时带的参数
            timeout: 5000,
            headers: {
                'X-Requested-With': 'XMLHttpRequest'
            }
        })
    }
}