import Vue from 'vue'
import App from './App'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import axios from '@/js/axios.js'

Vue.prototype.$axios = axios

Vue.use(ElementUI)

function onWindowLoad() {
  chrome.tabs.executeScript(null, {
    file: "getPagesSource.js"
  }, () => {
    // If you try and inject into an extensions page or the webstore/NTP you'll get an error
    if (chrome.runtime.lastError) {
      console.log('There was an error injecting script : \n' + chrome.runtime.lastError.message)
    }
  });
}

window.onload = onWindowLoad;

/* eslint-disable no-new */
new Vue({
  el: '#app',
  render: h => h(App)
})